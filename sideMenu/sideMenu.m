//
//  sideMenu.m
//  sideMenu
//
//  Created by Click Labs134 on 11/18/15.
//  Copyright © 2015 clicklabs. All rights reserved.
//

#import "sideMenu.h"

NSArray *menuArray;
UITableView *Menu;
UITableViewCell *cell;

@implementation sideMenu

@synthesize destdata;

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void) awakeFromNib
{
    menuArray=[[NSArray alloc]init];
    menuArray=[NSArray arrayWithObjects:@"Red",@"Blue",@"Green",@"Yellow", nil];
    
    Menu=[[UITableView alloc]init];
    Menu.frame=CGRectMake(20, 80, 145, 170);
    Menu.delegate=self;
    Menu.dataSource=self;
    [Menu registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    [self addSubview:Menu];
    Menu.backgroundColor=[UIColor blueColor];
    [Menu reloadData];
    
}
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return menuArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell=[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    
    cell.backgroundColor=[UIColor blueColor];
    cell.textLabel.text=menuArray[indexPath.row];
    cell.textColor=[UIColor whiteColor];
    cell.textLabel.font = [UIFont fontWithName:@"Futura" size:28.0];
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row==0)
    {
      [[UIView appearance] setBackgroundColor:[UIColor redColor]];
        
    }

}

@end
