//
//  ViewController.m
//  sideMenu
//
//  Created by Click Labs134 on 11/18/15.
//  Copyright © 2015 clicklabs. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (strong, nonatomic) IBOutlet sideMenu *sideView;

@end

@implementation ViewController
@synthesize sideView;

- (void)viewDidLoad {
    [super viewDidLoad];
    sideView.hidden=YES;

   
    // Do any additional setup after loading the view, typically from a nib.
}
- (IBAction)showMenu:(id)sender
{
    sideView.hidden=NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
